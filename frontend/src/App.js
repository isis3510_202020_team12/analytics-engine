import Navbar from "./components/Navbar";
import UsersAppointmentsTable from "./components/UsersAppointmentsTable";
import { useEffect, useState } from "react";
import AppointmentsPerMonth from "./components/AppointmentsPermonth";
import moment from "moment";
import "./App.css";
import HospitalVisits from "./components/HospitalVisits";
import BMITable from "./components/BMITable";
import BloodChart from "./components/BloodChart";
import WeightIssueChart from "./components/WeightIssuesChart";
import DiagnosisPerMonth from "./components/DiagnosisPerMonth";
import DiagnosisWordcloud from "./components/DiagnosisWordCloud";
import UsersDiagnosisTable from "./components/UsersDiagnosisTable";

function App() {
  const [users, setUsers] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [appointmentsPerMonth, setAppointmentsPerMonth] = useState([]);
  const [appointmentTypes, setAppointmentsTypes] = useState([]);
  const [hospitalVisits, setHospitalVisits] = useState([]);
  const [avgHeightM, setAvgHeightM] = useState(0);
  const [avgHeightF, setAvgHeightF] = useState(0);
  const [avgWeightM, setAvgWeightM] = useState(0);
  const [avgWeightF, setAvgWeightF] = useState(0);

  useEffect(() => {
    fetch("/users")
      .then((response) => response.json())
      .then((data) => {
        setUsers(data);
      });
    fetch("/appointments")
      .then((response) => response.json())
      .then((data) => {
        setAppointments(data);
      });
    fetch("/appointmentsPerMonth")
      .then((response) => response.json())
      .then((data) => {
        setAppointmentsPerMonth(data);
      });
    fetch("/appointmentTypes")
      .then((response) => response.json())
      .then((data) => {
        setAppointmentsTypes(data);
      });
    fetch("/appointmentsHospitals")
      .then((response) => response.json())
      .then((data) => {
        setHospitalVisits(data);
      });
    fetch("/usersHeight/M")
      .then((response) => response.json())
      .then((data) => {
        setAvgHeightM(data.avgHeight);
      });
    fetch("/usersHeight/F")
      .then((response) => response.json())
      .then((data) => {
        setAvgHeightF(data.avgHeight);
      });
    fetch("/usersWeight/M")
      .then((response) => response.json())
      .then((data) => {
        setAvgWeightM(data.avgWeight);
      });
    fetch("/usersWeight/F")
      .then((response) => response.json())
      .then((data) => {
        setAvgWeightF(data.avgWeight);
      });
  }, []);

  const getAppointmentType = (num) => {
    if (num === 0) {
      return "General";
    } else if (num === 1) {
      return "Urgencias";
    } else if (num === 2) {
      return "Prioritaria";
    } else if (num === 3) {
      return "Especializada";
    }
  };

  return (
    <div className="app">
      <Navbar />
      <div className="container">
        <h1 className="page-title">Analytics Dashboard</h1>
        <h2 className="dashboard-subtitle">Users Appointments</h2>
        <hr />
        <div className="container" id="appointments-average-container">
          <div className="row">
            <div className="col-4">
              <div className="card shadow" id="appointments-average-card">
                <div className="container">
                  <h1 id="appointments-average-card-title">
                    Average Appointments per User
                  </h1>
                  <div className="card-body" id="appointments-average">
                    {(appointments.length / users.length).toFixed(0)}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="card shadow" id="most-appointments-card">
                <div className="container">
                  <h1 id="most-appointments-card-title">
                    Month with most appointments
                  </h1>
                  <div className="card-body" id="most-appointments-month">
                    {moment()
                      .month(
                        appointmentsPerMonth.indexOf(
                          Math.max(...appointmentsPerMonth)
                        )
                      )
                      .format("MMMM")}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="card shadow" id="most-common-type-card">
                <div className="container">
                  <h1 id="most-common-type-card-title">
                    Most common type of appointment
                  </h1>
                  <div className="card-body" id="most-common-type">
                    {getAppointmentType(
                      appointmentTypes.indexOf(Math.max(...appointmentTypes)) +
                        1
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <h3 className="dashboard-subsubtitle">Appointments Per Month</h3>
              <AppointmentsPerMonth appointments={appointmentsPerMonth} />
            </div>
            <div className="col">
              <h3 className="dashboard-subsubtitle">Hospitals Visits</h3>
              <HospitalVisits hospitals={hospitalVisits} />
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <h3 className="dashboard-subsubtitle">Users</h3>
          <UsersAppointmentsTable users={users} />
        </div>
        <div className="container-fluid" id="user-basic-info-container">
          <h2 className="dashboard-subtitle">Users Basic Information</h2>
          <hr />
          <div className="container-fluid" id="user-basic-info-cards-container">
            <div className="row">
              <div className="col-3">
                <div className="card shadow user-info-data-card">
                  <div className="container">
                    <h1 className="user-info-data-card-title">Avg height 🧑</h1>
                    <div className="user-info-data">
                      {avgHeightM} <span className="units">cm</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-3">
                <div className="card shadow user-info-data-card">
                  <div className="container">
                    <h1 className="user-info-data-card-title">Avg height 👩‍🦰</h1>
                    <div className="user-info-data">
                      {avgHeightF} <span className="units">cm</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-3">
                <div className="card shadow user-info-data-card">
                  <div className="container">
                    <h1 className="user-info-data-card-title">Avg Weight 🧑</h1>
                    <div className="user-info-data">
                      {avgWeightM} <span className="units">kg</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-3">
                <div className="card shadow user-info-data-card">
                  <div className="container">
                    <h1 className="user-info-data-card-title">Avg Weight 👩‍🦰</h1>
                    <div className="user-info-data">
                      {avgWeightF} <span className="units">kg</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container-fluid" id="bmi-table-container">
            <h3 className="dashboard-subsubtitle">Users</h3>
            <BMITable users={users} />
          </div>
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h3 className="dashboard-subsubtitle">Blood Type</h3>
                <BloodChart />
              </div>
              <div className="col">
                <h3 className="dashboard-subsubtitle">Weight Issues</h3>
                <WeightIssueChart />
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <h2 className="dashboard-subtitle">Users Diagnosis</h2>
          <hr />
          <div className="container-fluid">
            <div className="row">
              <div className="col">
                <h3 className="dashboard-subsubtitle">Diagnosis Per Month</h3>
                <DiagnosisPerMonth />
              </div>
              <div className="col">
                <h3 className="dashboard-subsubtitle">Diagnosis</h3>
                <DiagnosisWordcloud />
              </div>
            </div>
            <div className="container-fluid">
              <h3 className="dashboard-subsubtitle">Users</h3>
              <UsersDiagnosisTable users={users} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
