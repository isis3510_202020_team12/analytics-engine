import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const HospitalVisits = (props) => {
  const data = [
    {
      name: "Santa Fe",
      numberOfVisits: props.hospitals[0],
    },
    {
      name: "Clínica del Country",
      numberOfVisits: props.hospitals[1],
    },
    {
      name: "Reina Sofía",
      numberOfVisits: props.hospitals[2],
    },
  ];

  return (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 15,
      }}
    >
      <CartesianGrid strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="numberOfVisits"
        stroke="#8884d8"
        activeDot={{ r: 8 }}
        strokeWidth={2}
      />
    </LineChart>
  );
};

export default HospitalVisits;
