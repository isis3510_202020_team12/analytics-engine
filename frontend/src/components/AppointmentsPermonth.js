import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const AppointmentsPerMonth = (props) => {
  const data = [
    {
      name: "Jan",
      numberOfAppointments: props.appointments[0],
    },
    {
      name: "Feb",
      numberOfAppointments: props.appointments[1],
    },
    {
      name: "Mar",
      numberOfAppointments: props.appointments[2],
    },
    {
      name: "Apr",
      numberOfAppointments: props.appointments[3],
    },
    {
      name: "May",
      numberOfAppointments: props.appointments[4],
    },
    {
      name: "Jun",
      numberOfAppointments: props.appointments[5],
    },
    {
      name: "Jul",
      numberOfAppointments: props.appointments[6],
    },
    {
      name: "Aug",
      numberOfAppointments: props.appointments[7],
    },
    {
      name: "Sep",
      numberOfAppointments: props.appointments[8],
    },
    {
      name: "Oct",
      numberOfAppointments: props.appointments[9],
    },
    {
      name: "Nov",
      numberOfAppointments: props.appointments[10],
    },
    {
      name: "Dec",
      numberOfAppointments: props.appointments[11],
    },
  ];

  return (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 15,
      }}
    >
      <CartesianGrid strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="numberOfAppointments"
        stroke="#8884d8"
        activeDot={{ r: 8 }}
        strokeWidth={2}
      />
    </LineChart>
  );
};

export default AppointmentsPerMonth;
