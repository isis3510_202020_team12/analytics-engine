import { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const WeightIssuesChart = () => {
  const [weightIssuesM, setWeightIsuuesM] = useState([]);
  const [weightIssuesF, setWeightIsuuesF] = useState([]);

  useEffect(() => {
    fetch("/usersWeightIssues/M")
      .then((response) => response.json())
      .then((data) => {
        setWeightIsuuesM(data);
      });
    fetch("/usersWeightIssues/F")
      .then((response) => response.json())
      .then((data) => {
        setWeightIsuuesF(data);
      });
  }, []);

  let data = [
    {
      name: "Underweight",
      M: weightIssuesM[0],
      F: weightIssuesF[0],
    },
    {
      name: "Normal Weight",
      M: weightIssuesM[1],
      F: weightIssuesF[1],
    },
    {
      name: "Overweight",
      M: weightIssuesM[2],
      F: weightIssuesF[2],
    },
    {
      name: "Obese",
      M: weightIssuesM[3],
      F: weightIssuesF[3],
    },
  ];

  return (
    <BarChart
      width={600}
      height={300}
      data={data}
      margin={{
        top: 20,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="M" stackId="a" fill="#8884d8" />
      <Bar dataKey="F" stackId="a" fill="#82ca9d" />
    </BarChart>
  );
};

export default WeightIssuesChart;
