import { useEffect, useState } from "react";
import { PieChart, Pie, Legend, Tooltip, Cell } from "recharts";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

const BloodChart = () => {
  const [bloodTypes, setBloodTypes] = useState([]);

  useEffect(() => {
    fetch("/usersBloodtype")
      .then((response) => response.json())
      .then((data) => {
        setBloodTypes(data);
      });
  }, []);

  let data = [
    { name: "A+", value: bloodTypes[0] },
    { name: "A-", value: bloodTypes[1] },
    { name: "B+", value: bloodTypes[2] },
    { name: "B-", value: bloodTypes[3] },
    { name: "AB+", value: bloodTypes[4] },
    { name: "AB-", value: bloodTypes[5] },
    { name: "O+", value: bloodTypes[6] },
    { name: "O-", value: bloodTypes[7] },
  ];

  return (
    <PieChart width={400} height={400}>
      <Legend verticalAlign="bottom" height={36} />
      <Pie
        dataKey="value"
        isAnimationActive={true}
        data={data}
        cx={200}
        cy={200}
        outerRadius={120}
        fill="#8884d8"
        label
      >
        {data.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
      <Tooltip />
    </PieChart>
  );
};

export default BloodChart;
