const TableRow2 = (props) => {
  const renderBadge2 = (bmi) => {
    if (25 > bmi && bmi > 18) {
      return <span className="badge badge-success">Normal weight</span>;
    } else if (bmi < 18) {
      return <span className="badge badge-secondary">Underweight</span>;
    } else if (bmi > 25 && bmi < 30) {
      return <span className="badge badge-warning">Overweight</span>;
    } else if (bmi > 30) {
      return <span className="badge badge-danger">Obese</span>;
    }
  };

  return (
    <tr>
      <th>{props.user.id}</th>
      <td>{props.user.name}</td>
      <td>{props.user.lastName}</td>
      <td>{props.user.weight + " kg"}</td>
      <td>{props.user.height + " cm"}</td>
      <td>
        {(props.user.weight / Math.pow(props.user.height / 100, 2)).toFixed(2)}
      </td>
      <td>
        {renderBadge2(props.user.weight / Math.pow(props.user.height / 100, 2))}
      </td>
    </tr>
  );
};

export default TableRow2;
