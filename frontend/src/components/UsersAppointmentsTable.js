import TableRow from "./TableRow";

const UsersAppointmentsTable = (props) => {
  const renderUsers = () => {
    return props.users.map((user) => {
      return <TableRow user={user} key={user.id} />;
    });
  };

  return (
    <div>
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">User Id</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Last Appointment Date</th>
            <th scope="col"># of Appointments</th>
            <th scope="col">Days past since last appointment</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>{renderUsers()}</tbody>
      </table>
    </div>
  );
};

export default UsersAppointmentsTable;
