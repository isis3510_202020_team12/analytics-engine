import moment from "moment";
import { useEffect, useState } from "react";

const TableRow = (props) => {
  const [appointmentsNumber, setAppointmentsNumber] = useState(0);

  const [lastAppointment, setlastappointment] = useState(0);

  useEffect(() => {
    fetch("/appointments/" + props.user.id)
      .then((response) => response.json())
      .then((data) => {
        setAppointmentsNumber(data.length);
        setlastappointment(data[0].date._seconds);
      });
  }, []);

  const renderBadge = (days) => {
    if (days > 60) {
      return <span className="badge badge-primary">Suggest Appointment</span>;
    }
  };

  return (
    <tr>
      <th>{props.user.id}</th>
      <td>{props.user.name}</td>
      <td>{props.user.lastName}</td>
      <td>{moment.unix(lastAppointment).format("MMM D, YYYY")}</td>
      <td>{appointmentsNumber}</td>
      <td>{moment().diff(moment.unix(lastAppointment), "days")}</td>
      <td>
        {renderBadge(moment().diff(moment.unix(lastAppointment), "days"))}
      </td>
    </tr>
  );
};

export default TableRow;
