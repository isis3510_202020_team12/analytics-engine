import { useEffect, useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const DiagnosisPerMonth = () => {
  const [diagnosisPerMonth, setDiagnosisPerMonth] = useState([]);

  useEffect(() => {
    fetch("/diagnosisPerMonth")
      .then((response) => response.json())
      .then((data) => {
        setDiagnosisPerMonth(data);
      });
  }, []);

  const data = [
    {
      name: "Jan",
      numberOfDiagnosis: diagnosisPerMonth[0],
    },
    {
      name: "Feb",
      numberOfDiagnosis: diagnosisPerMonth[1],
    },
    {
      name: "Mar",
      numberOfDiagnosis: diagnosisPerMonth[2],
    },
    {
      name: "Apr",
      numberOfDiagnosis: diagnosisPerMonth[3],
    },
    {
      name: "May",
      numberOfDiagnosis: diagnosisPerMonth[4],
    },
    {
      name: "Jun",
      numberOfDiagnosis: diagnosisPerMonth[5],
    },
    {
      name: "Jul",
      numberOfDiagnosis: diagnosisPerMonth[6],
    },
    {
      name: "Aug",
      numberOfDiagnosis: diagnosisPerMonth[7],
    },
    {
      name: "Sep",
      numberOfDiagnosis: diagnosisPerMonth[8],
    },
    {
      name: "Oct",
      numberOfDiagnosis: diagnosisPerMonth[9],
    },
    {
      name: "Nov",
      numberOfDiagnosis: diagnosisPerMonth[10],
    },
    {
      name: "Dec",
      numberOfDiagnosis: diagnosisPerMonth[11],
    },
  ];

  return (
    <LineChart
      width={500}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 15,
      }}
    >
      <CartesianGrid strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="numberOfDiagnosis"
        stroke="#8884d8"
        activeDot={{ r: 8 }}
        strokeWidth={2}
      />
    </LineChart>
  );
};

export default DiagnosisPerMonth;
