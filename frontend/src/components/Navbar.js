import "../styles/Navbar.css";
const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg shadow">
      <h1 className="navbar-brand">MedRecord</h1>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav"></ul>
      </div>
    </nav>
  );
};

export default Navbar;
