import TableRow2 from "./TableRow2";

const BMITable = (props) => {
  const renderUsers = () => {
    return props.users.map((user) => {
      return <TableRow2 user={user} key={user.id} />;
    });
  };

  return (
    <div>
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">User Id</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Weight</th>
            <th scope="col">Height</th>
            <th scope="col">BMI</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>{renderUsers()}</tbody>
      </table>
    </div>
  );
};

export default BMITable;
