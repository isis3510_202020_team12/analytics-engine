import TableRow3 from "./TableRow3";

const UsersAppointmentsTable = (props) => {
  const renderUsers = () => {
    return props.users.map((user) => {
      return <TableRow3 user={user} key={user.id} />;
    });
  };

  return (
    <div>
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">User Id</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col"># of Diagnosis</th>
            <th scope="col"># of Current Diagnosis</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>{renderUsers()}</tbody>
      </table>
    </div>
  );
};

export default UsersAppointmentsTable;
