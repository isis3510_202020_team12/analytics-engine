import { useEffect, useState } from "react";
import ReactWordcloud from "react-wordcloud";

const options = {
  rotations: 2,
  rotationAngles: [0],
};
const size = [500, 100];

function DiagnosisWordCloud() {
  const [wordsObject, setWordsObject] = useState({});

  useEffect(() => {
    fetch("/diagnosisAll")
      .then((response) => response.json())
      .then((data) => {
        let wordsTemp = [];
        Object.entries(data).forEach((ent) => {
          wordsTemp.push({
            text: ent[0],
            value: ent[1],
          });
        });
        setWords(wordsTemp);
      });
  }, []);

  const [words, setWords] = useState([
    {
      text: "told",
      value: 64,
    },
  ]);

  return (
    <div>
      <ReactWordcloud options={options} size={size} words={words} />
    </div>
  );
}

export default DiagnosisWordCloud;
