import { useEffect, useState } from "react";

const TableRow3 = (props) => {
  const [diagnosis, setDiagnosis] = useState([]);

  useEffect(() => {
    fetch("/diagnosis/" + props.user.id)
      .then((response) => response.json())
      .then((data) => {
        setDiagnosis(data);
      });
  }, []);

  const renderBadge = () => {
    if (diagnosis.filter((diag) => diag.name === "Alcoholism").length !== 0) {
      return (
        <span className="badge badge-warning">Substance Abuse Problem</span>
      );
    }
  };

  return (
    <tr>
      <th>{props.user.id}</th>
      <td>{props.user.name}</td>
      <td>{props.user.lastName}</td>
      <td>{diagnosis.length}</td>
      <td>{diagnosis.filter((diag) => diag.isCurrent === true).length}</td>
      <td>{renderBadge()}</td>
    </tr>
  );
};

export default TableRow3;
