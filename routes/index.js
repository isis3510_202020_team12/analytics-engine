var express = require("express");
var router = express.Router();
const admin = require("firebase-admin");
var moment = require("moment");

let serviceAccount = require("../public/firebase-key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

let db = admin.firestore();

/* GET users. */
router.get("/users", function (req, res, next) {
  let users = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        users.push({
          ...doc.data(),
          id: doc.id,
        });
      });
      res.send(users);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/* GET usersWeight. */
router.get("/usersWeight/:gender", function (req, res, next) {
  let weight = 0;
  let users = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        if (doc.data().gender === req.params.gender) {
          users.push({
            id: doc.id,
          });
          weight = weight + parseInt(doc.data().weight);
        }
      });
      res.send({ avgWeight: weight / users.length });
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/* GET usersWeight. */
router.get("/usersHeight/:gender", function (req, res, next) {
  let height = 0;
  let users = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        if (doc.data().gender === req.params.gender) {
          users.push({
            id: doc.id,
          });
          height = height + parseInt(doc.data().height);
        }
      });
      res.send({ avgHeight: height / users.length });
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/* GET usersWeight. */
router.get("/usersWeightIssues/:gender", function (req, res, next) {
  let weightIssues = [0, 0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        if (doc.data().gender === req.params.gender) {
          let userbmi =
            doc.data().weight / Math.pow(doc.data().height / 100, 2);
          if (userbmi < 18) {
            weightIssues[0] = weightIssues[0] + 1;
          } else if (userbmi > 18 && userbmi < 25) {
            weightIssues[1] = weightIssues[1] + 1;
          } else if (userbmi > 25 && userbmi < 30) {
            weightIssues[2] = weightIssues[2] + 1;
          } else if (userbmi > 30) {
            weightIssues[3] = weightIssues[3] + 1;
          }
        }
      });
      res.send(weightIssues);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/* GET usersBloodtype. */
router.get("/usersBloodtype", function (req, res, next) {
  let bloodTypes = [0, 0, 0, 0, 0, 0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        if (doc.data().blood === "A+") {
          bloodTypes[0] = bloodTypes[0] + 1;
        } else if (doc.data().blood === "A-") {
          bloodTypes[1] = bloodTypes[1] + 1;
        } else if (doc.data().blood === "B+") {
          bloodTypes[2] = bloodTypes[2] + 1;
        } else if (doc.data().blood === "B-") {
          bloodTypes[3] = bloodTypes[3] + 1;
        } else if (doc.data().blood === "AB+") {
          bloodTypes[4] = bloodTypes[4] + 1;
        } else if (doc.data().blood === "AB-") {
          bloodTypes[5] = bloodTypes[5] + 1;
        } else if (doc.data().blood === "O+") {
          bloodTypes[6] = bloodTypes[6] + 1;
        } else if (doc.data().blood === "O-") {
          bloodTypes[7] = bloodTypes[7] + 1;
        }
      });
      res.send(bloodTypes);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get Appointments*/
router.get("/appointments", function (req, res, next) {
  let appointments = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref.collection("appointments").orderBy("date", "desc").get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          appointments.push(subDoc2.data());
        });
      });
      res.send(appointments);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get Appointments by id*/
router.get("/appointments/:id", function (req, res, next) {
  let appointments = [];
  db.collection("users")
    .doc(req.params.id)
    .get()
    .then((snapshot) => {
      let getAp = snapshot.ref
        .collection("appointments")
        .orderBy("date", "desc")
        .get();
      return getAp;
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        appointments.push(subDoc.data());
      });
      res.send(appointments);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get AppointmentsPerMonth*/
router.get("/appointmentsPerMonth", function (req, res, next) {
  let appointmentsPerMonth = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref.collection("appointments").orderBy("date", "desc").get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          appointmentsPerMonth[
            moment.unix(subDoc2.data().date._seconds).month()
          ] =
            appointmentsPerMonth[
              moment.unix(subDoc2.data().date._seconds).month()
            ] + 1;
        });
      });
      res.send(appointmentsPerMonth);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get AppointmentsTypes*/
router.get("/appointmentsTypes", function (req, res, next) {
  let appointmentsTypes = [0, 0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref.collection("appointments").orderBy("date", "desc").get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          if (subDoc2.data().type === "General") {
            appointmentsTypes[0] = appointmentsTypes[0] + 1;
          } else if (subDoc2.data().type === "Urgencias") {
            appointmentsTypes[1] = appointmentsTypes[1] + 1;
          } else if (subDoc2.data().type === "Prioritaria") {
            appointmentsTypes[2] = appointmentsTypes[2] + 1;
          } else if (subDoc2.data().type === "Especializada") {
            appointmentsTypes[3] = appointmentsTypes[3] + 1;
          }
        });
      });
      res.send(appointmentsTypes);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get AppointmentsHospitals*/
router.get("/appointmentsHospitals", function (req, res, next) {
  let appointmentsTypes = [0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref.collection("appointments").orderBy("date", "desc").get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          if (subDoc2.data().hospital === "Santa Fe") {
            appointmentsTypes[0] = appointmentsTypes[0] + 1;
          } else if (subDoc2.data().hospital === "Clínica del Country") {
            appointmentsTypes[1] = appointmentsTypes[1] + 1;
          } else if (subDoc2.data().hospital === "Reina Sofía") {
            appointmentsTypes[2] = appointmentsTypes[2] + 1;
          }
        });
      });
      res.send(appointmentsTypes);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get Diagnosis*/
router.get("/diagnosis", function (req, res, next) {
  let diagnosis = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref.collection("diagnosis").get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          diagnosis.push(subDoc2.data());
        });
      });
      res.send(diagnosis);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get DiagnosisPerMonth*/
router.get("/diagnosisPerMonth", function (req, res, next) {
  let diagnosisPerMonth = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref
          .collection("diagnosis")
          .orderBy("registrationDate", "desc")
          .get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          diagnosisPerMonth[
            moment.unix(subDoc2.data().registrationDate._seconds).month()
          ] =
            diagnosisPerMonth[
              moment.unix(subDoc2.data().registrationDate._seconds).month()
            ] + 1;
        });
      });
      res.send(diagnosisPerMonth);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get DiagnosisPerMonth*/
router.get("/diagnosisAll", function (req, res, next) {
  let diagnosisAll = [];
  db.collection("users")
    .get()
    .then((snapshot) => {
      let getAp = snapshot.docs.map((doc) =>
        doc.ref
          .collection("diagnosis")
          .orderBy("registrationDate", "desc")
          .get()
      );
      return Promise.all(getAp);
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        subDoc.forEach((subDoc2) => {
          diagnosisAll.push(subDoc2.data().name);
        });
      });
      var count = {};
      diagnosisAll.forEach(function (i) {
        count[i] = (count[i] || 0) + 1;
      });
      res.send(count);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

/*Get Diagnosis by id*/
router.get("/diagnosis/:id", function (req, res, next) {
  let diagnosis = [];
  db.collection("users")
    .doc(req.params.id)
    .get()
    .then((snapshot) => {
      let getAp = snapshot.ref.collection("diagnosis").get();
      return getAp;
    })
    .then((snapshot2) => {
      snapshot2.forEach((subDoc) => {
        diagnosis.push(subDoc.data());
      });
      res.send(diagnosis);
    })
    .catch((err) => {
      console.log("Error getting documents", err);
    });
});

module.exports = router;
